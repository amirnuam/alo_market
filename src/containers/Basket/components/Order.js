import React from 'react';
import {View,StyleSheet,Text} from 'react-native';
import {Color} from './../../../../src/constants/color';
import {DEVICE_HEIGH, DEVICE_WIDTH} from './../../../../src/constants/common';

export const Order = ({data : {id , region , status}}) => {


    return (
        <View style={s.container}>
            <Text>Status : {status}</Text>
            <Text>id : {id}</Text>
            <Text>longitude : {region.longitude}</Text>
            <Text>latitude : {region.latitude}</Text>
        </View>
    );
};

const s = StyleSheet.create({
    container:{
        width:DEVICE_WIDTH * 0.9,
        flexDirection:"column",
        justifyContent:"center",
        alignItems:"center",
        height:DEVICE_HEIGH/3,
        flex: 1,
        borderColor:Color.black,
        borderRadius:30
    },
});
