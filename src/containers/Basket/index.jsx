import React, {useState, useEffect} from 'react';
import {ScrollView, AsyncStorage,SafeAreaView} from 'react-native';
import {Order} from './components/Order';
import {useInterval} from "./../../services/intervalService"

export const Basket = () => {

    const [orders, setOrders] = useState([]);


    const getFreshOrders = async () => {
        const freshOrders = JSON.parse(await AsyncStorage.getItem('orders')) || [];
        setOrders(freshOrders);
    };

    const changeOrdersStatus = async () => {
        const orders = JSON.parse(await AsyncStorage.getItem('orders')) || [];
        const newOrders = [];
        orders.forEach(order=>{
            newOrders.push({
                ...order,
                status : Math.random().toString(36).substring(7)
            })
        });
        await AsyncStorage.setItem("orders",JSON.stringify(newOrders))
    };

    useInterval(()=>{
        getFreshOrders();
    },5000);

    useInterval(()=>{
        changeOrdersStatus();
    },30000);




    useEffect(() => {
        getFreshOrders();
    }, []);


    return (
       <SafeAreaView style={{justifyContent:"center",alignItems:"center"}}>
           <ScrollView>
               <>
                   {
                       orders.map(order => (
                           <Order key={order.id} data={order}/>
                       ))
                   }
               </>
           </ScrollView>
       </SafeAreaView>
    );
};



