import React from 'react';
import {ScrollView,SafeAreaView} from 'react-native';
import {CATEGORIES} from './../../../src/constants/common';
import {Category} from './components/Category';

export const Shop = ({navigation}) => {

    const RenderCategory = ()=>(
        <ScrollView>
            {
                CATEGORIES.map(category => (
                    <Category navigation={navigation} key={category.id} data={category}/>
                ))
            }
        </ScrollView>
    );


    return (
        <SafeAreaView>
           <RenderCategory/>
        </SafeAreaView>
    );
};
