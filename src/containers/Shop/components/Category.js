import React from 'react';
import {View, ScrollView, StyleSheet,Text} from 'react-native';
import {Color} from './../../../../src/constants/color';
import {Product} from './Product';
import {DEVICE_HEIGH, DEVICE_WIDTH, PRODUCTS} from './../../../../src/constants/common';

export const Category = ({data: {id,name},navigation}) => {

    const products = PRODUCTS.filter(product => product.categoryId === id);

    const RenderProducts = () => (
        <ScrollView horizontal={true}>
            {
                products.map(product => (
                        <Product navigation={navigation} key={product.id} data={product}/>
                ))
            }
        </ScrollView>
    );

    return (
        <View style={s.container}>
            <View style={s.header}>
                <Text style={s.headerText}>{name}</Text>
            </View>
            <RenderProducts/>
        </View>
    );

};

const s = StyleSheet.create({
    container: {
        height: DEVICE_HEIGH / 5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    header :{
        height: 40 ,
        width:DEVICE_WIDTH,
        backgroundColor : Color.gray,
        justifyContent: "center",
        alignItems: "center",
        margin:5,
    },
    headerText:{
        color : "#fff",
        fontSize:21
    }
});
