import React, {useState} from 'react';
import {StyleSheet, View,Text,TouchableOpacity} from 'react-native';
import {DEVICE_WIDTH} from './../../../constants/common';
import {Color} from './../../../constants/color';
import MapView from "react-native-maps";

export const Location = ({onSubmit = () => {}}) => {

    const [region , setRegion] = useState({
        latitude: 35.6892,
        longitude: 51.3790,
        latitudeDelta: 0.001,
        longitudeDelta: 0.001
    });

    const onRegionChange = region => {
        setRegion(region);
    };
    const onLocationSelect = () => {
        onSubmit(region);
    };

    return  (
        <View style={styles.container}>
            <View style={styles.wrapper2}>
                <MapView
                    style={styles.map}
                    initialRegion={region}
                    showsUserLocation={true}
                    onRegionChangeComplete={onRegionChange}
                >
                </MapView>
                <View style={styles.mapMarkerContainer}>
                    <Text name={"apple"} style={styles.marker} >🛵</Text>
                </View>
            </View>
            <View style={styles.pickButton}>
                <TouchableOpacity
                    onPress={onLocationSelect}
                    style={styles.button}
                >
                    <Text style={styles.buttonText}>pick delivery location</Text>
                </TouchableOpacity>
            </View>

        </View>
    );


};



const styles = StyleSheet.create({
    wrapper: {
        paddingTop: 40,
        paddingBottom: 25,
        justifyContent:"center",
        alignItems:"center"
    },
    wrapper2:{ flex: 2 , marginTop:50},
    marker:{ fontSize: 42, color: Color.gray },
    container: {
        display: "flex",
        height: "60%",
        backgroundColor:Color.white,
        width: "100%",
        borderRadius: 30
    },
    pickButton:{width:"100%",height: "50%",justifyContent:"center", alignItems:"center"},
    map: {
        flex: 1
    },
    mapMarkerContainer: {
        left: '47%',
        position: 'absolute',
        top: '42%'
    },
    mapMarker: {
        fontSize: 40,
        color: "red"
    },
    deatilSection: {
        flex: 1,
        backgroundColor: "#fff",
        padding: 10,
        display: "flex",
        justifyContent: "center"
    },
    spinnerView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    btnContainer: {
        width: DEVICE_WIDTH - 20,
        position: "absolute",
        bottom: 100,
        left: 10
    },
    button:{
        borderRadius:20,
        borderWidth:1,
        borderColor:Color.black
    },
    buttonText:{
        padding : 15
    }
});
