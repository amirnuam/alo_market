import React, {useState} from 'react';
import {TouchableOpacity, Text, StyleSheet, AsyncStorage} from 'react-native';
import {Location} from './Location';
import {Color} from './../../../../src/constants/color';
import {DEVICE_WIDTH} from './../../../../src/constants/common';
import Modal from 'react-native-modal';

export const Product = ({data: {name, id} , navigation}) => {
    const [isModalVisible, setModalVisible] = useState(false);

    const toggleModal = () => {
        setModalVisible(!isModalVisible);
    };

    const onSubmit = async (region) => {

        let orders = JSON.parse(await AsyncStorage.getItem('orders')) || [];
        const orderId = new Date().getTime();
        await AsyncStorage.setItem('orders', JSON.stringify([...orders, {
            region,
            productId: id,
            id: orderId,
            status: 'sent',
        }]));
        navigation.navigate('Basket');
        setModalVisible(false);
    };

    return (
        <>
            <TouchableOpacity onPress={toggleModal} style={s.container}>
                <Text>{name}</Text>
            </TouchableOpacity>
            <Modal isVisible={isModalVisible}>
                <Location onSubmit={onSubmit}/>
            </Modal>
        </>
    );

};

const s = StyleSheet.create({
    container: {
        margin: 10,
        width: DEVICE_WIDTH / 5,
        height: '50%',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 15,
        borderWidth: 1,
        borderColor: Color.black,
    },
});
