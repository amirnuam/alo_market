import {Dimensions} from 'react-native';

export const CATEGORIES = [
    {
        id : 1 ,
        name : "digital"
    },
    {
        id : 2 ,
        name : "cloths"
    },
    {
        id : 3 ,
        name : "camping"
    }
];

export const PRODUCTS = [
    {
        id : 1 ,
        categoryId : 1 ,
        name : "phone"
    },
    {
        id : 2 ,
        categoryId : 1 ,
        name : "gard"
    },
    {
        id : 3 ,
        categoryId : 1 ,
        name : "glass"
    },
    {
        id : 4 ,
        categoryId : 2 ,
        name : "socks"
    },
    {
        id : 5 ,
        categoryId : 2 ,
        name : "cap"
    },
    {
        id : 6 ,
        categoryId : 3 ,
        name : "knife"
    }
];



export const DEVICE_WIDTH = Dimensions.get('window').width;

export const DEVICE_HEIGH = Dimensions.get('window').height;
