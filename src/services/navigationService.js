import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {Basket,Shop} from "./../containers/index"


export const RegisterNavigation = ()=>{
    const Tab = createBottomTabNavigator();

    return(
        <NavigationContainer>
            <Tab.Navigator>
                <Tab.Screen name="Shop" component={Shop} options={{ title: 'Shop' }}/>
                <Tab.Screen name="Basket" component={Basket}  options={{ title: 'Basket' }} />
            </Tab.Navigator>
        </NavigationContainer>
    );
};
